package pageobjects;

import com.google.common.base.Strings;
import dto.JobBasicDTO;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.stream.Collectors;

public class JobResultsPanel extends AbstractPageObject {

    // Search results panel
    private static final By RESULTS_NUMBER = By.className("total_results");
    private static final By RESULTS_LIST = By.id("job_results_list_hldr");
    private static final By RESULTS_LIST_ELEMENT = By.className("jlr_right_hldr");
    private static final By NO_RESULTS_MESSAGE = By.className("no_results_text");

    private static final By RESULT_ELEMENT_TITLE = By.className("job_link");
    private static final By RESULT_ELEMENT_LOCATION = By.className("location");
    private static final By RESULT_ELEMENT_CATEGORY = By.className("category");
    private static final By RESULT_ELEMENT_DESCRIPTION = By.className("jlr_description");

    private static final Logger LOG = LoggerFactory.getLogger(JobResultsPanel.class.getSimpleName());

    public boolean isNoResultsMessageDisplayed() {
        LOG.info("Verifying if No Results Message is displayed...");
        boolean displayed = isElementDisplayed(NO_RESULTS_MESSAGE);
        LOG.info("Displayed: {}", displayed);
        return displayed;
    }

    public int getNumberOfResults() {
        LOG.info("Reading number of results...");
        String resultsNumber = getWebElement(RESULTS_NUMBER).getText();
        LOG.info("Number of results is: '{}'", resultsNumber);
        return Strings.isNullOrEmpty(resultsNumber) ? 0 : Integer.parseInt(resultsNumber);
    }

    public List<JobBasicDTO> getResultJobs() {
        LOG.info("Reading job results");
        return getWebElement(RESULTS_LIST)
                .findElements(RESULTS_LIST_ELEMENT)
                .stream()
                .map(this::getJobDescription)
                .collect(Collectors.toList());
    }

    private JobBasicDTO getJobDescription(WebElement jobElement) {
        JobBasicDTO jobDescription = new JobBasicDTO();

        jobDescription.setJobTitle(jobElement.findElement(RESULT_ELEMENT_TITLE).getText());
        jobDescription.setLocation(jobElement.findElement(RESULT_ELEMENT_LOCATION).getText());
        jobDescription.setCategory(jobElement.findElement(RESULT_ELEMENT_CATEGORY).getText());
        jobDescription.setDescription(jobElement.findElement(RESULT_ELEMENT_DESCRIPTION).getText());
        return jobDescription;
    }
}
