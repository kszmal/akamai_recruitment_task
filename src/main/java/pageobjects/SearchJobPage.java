package pageobjects;

import lombok.Getter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SearchJobPage extends AbstractPageObject {

    // Search criteria panel
    private static final By JOB_TITLE_INPUT = By.id("keyword");
    private static final By LOCATION_DIV = By.id("jLocInputHldr");
    private static final By LOCATIONS_GROUP = By.className("chzn-results");
    private static final By SEARCH_BUTTON = By.id("jSearchSubmit");

    private static final Logger LOG = LoggerFactory.getLogger(SearchJobPage.class.getSimpleName());

    @Getter(lazy = true) private final JobResultsPanel jobResultsPanel = new JobResultsPanel();

    public void enterSearchKeyword(String keyword) {
        LOG.info("Entering search keyword: '{}'", keyword);
        sendKeys(JOB_TITLE_INPUT, keyword);
    }

    public void selectLocation(String locationName) {
        LOG.info("Selecting location: '{}'", locationName);
        click(LOCATION_DIV);
        WebElement locationElement = getWebElement(LOCATIONS_GROUP)
                .findElements(By.tagName("li"))
                .stream()
                .filter(location -> location.getText().equals(locationName))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("Location wasn't found: " + locationName));
        scrollToElement(locationElement);
        locationElement.click();
    }

    public void clickSearch() {
        LOG.info("Executing search of jobs");
        click(SEARCH_BUTTON);
        getFluentWait().until(driver -> "search_btn".equals(driver.findElement(SEARCH_BUTTON).getAttribute("class")));
        LOG.info("Search finished");
    }

    public void openMainPage() {
        LOG.info("Opening main page");
        openApp();
        LOG.info("Main page opened");
    }
}
