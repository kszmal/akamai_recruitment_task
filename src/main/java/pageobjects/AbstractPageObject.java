package pageobjects;

import core.ConfController;
import core.DriverFactory;
import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.SECONDS;

abstract class AbstractPageObject {

    @Getter(lazy = true) private final WebDriver webDriver = configureDriver();
    @Getter(lazy = true) private final Wait<WebDriver> fluentWait = new FluentWait<>(getWebDriver())
            .withTimeout(5, SECONDS)
            .pollingEvery(100, MILLISECONDS)
            .ignoring(NoSuchElementException.class);

    void openApp() {
        getWebDriver().get(ConfController.getProperty(ConfController.APP_URL_KEY));
    }

    WebElement getWebElement(By elementLocation) {
        return getFluentWait().until(driver -> driver.findElement(elementLocation));
    }

    @java.lang.SuppressWarnings("squid:S1166")
    //reason: in some cases exceptions thrown here is expected, do not log it
    boolean isElementDisplayed(By elementLocation) {
        try {
            return getWebDriver().findElement(elementLocation).isDisplayed();
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    void scrollToElement(WebElement element) {
        ((JavascriptExecutor) getWebDriver()).executeScript("arguments[0].scrollIntoView(true);", element);
    }

    void click(By elementLocation) {
        getWebElement(elementLocation).click();
    }

    void sendKeys(By elementLocation, String text) {
        getWebElement(elementLocation).sendKeys(text);
    }

    private WebDriver configureDriver() {
        WebDriver driver = DriverFactory.getInstance().getDriver();
        driver.manage().timeouts().pageLoadTimeout(10, SECONDS);
        return driver;
    }
}
