package dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class JobBasicDTO extends DtoObject {
    private String jobId;
    private String jobTitle;
    private String location;
    private String category;
    private String description;
}
