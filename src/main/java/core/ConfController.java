package core;

import org.apache.commons.lang3.ObjectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public final class ConfController {

    public static final String APP_URL_KEY = "appUrl";
    static final String BROWSER_KEY = "browser";
    static final String REMOTE_KEY = "remote";
    static final String SELENIUM_GRID = "seleniumGrid";
    private static final String CONF_FILE = "conf.properties";
    private static final Properties PROPERTIES = loadConfProperties();
    private static final Logger LOG = LoggerFactory.getLogger(ConfController.class.getSimpleName());

    private ConfController() {
        throw new IllegalStateException();
    }

    public static String getProperty(String propertyName) {
        return ObjectUtils.firstNonNull(getEnvironmentProperty(propertyName), PROPERTIES.getProperty(propertyName));
    }

    private static Properties loadConfProperties() {
        Properties properties = new Properties();
        try (InputStream inputStream = ClassLoader.getSystemResourceAsStream(CONF_FILE)) {
            properties.load(inputStream);
            return properties;
        } catch (IOException e) {
            LOG.error("Cannot load '{}' file", CONF_FILE, e);
            throw new IllegalArgumentException("Cannot load configuration file: " + CONF_FILE, e);
        }
    }

    private static String getEnvironmentProperty(String propertyName) {
        return System.getProperty(propertyName);
    }
}
