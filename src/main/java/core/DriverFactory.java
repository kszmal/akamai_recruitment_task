package core;

import com.google.common.collect.ImmutableMap;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;

public final class DriverFactory {
    private static final DriverFactory INSTANCE = new DriverFactory();
    private static final Logger LOG = LoggerFactory.getLogger(DriverFactory.class.getSimpleName());
    private static final ImmutableMap<String, Class<? extends RemoteWebDriver>> BROWSER_DRIVERS = ImmutableMap.of("firefox", FirefoxDriver.class,
            "chrome", ChromeDriver.class);

    private final ThreadLocal<RemoteWebDriver> drivers = ThreadLocal.withInitial(this::initDriver);

    private DriverFactory() {
        // do nothing
    }

    public static DriverFactory getInstance() {
        return INSTANCE;
    }

    public WebDriver getDriver() {
        return drivers.get();
    }

    public void removeDriver() {
        drivers.get().close();
        drivers.remove();
    }

    private RemoteWebDriver initDriver() {
        if ("true".equals(ConfController.getProperty(ConfController.REMOTE_KEY))) {
            return initRemoteWebDriver();
        } else {
            return initLocalDriver();
        }
    }

    private RemoteWebDriver initLocalDriver() {
        Class<? extends RemoteWebDriver> driverClass = setUpDriverInstance();
        try {
            DesiredCapabilities capabilities = DesiredCapabilities.firefox();
            capabilities.setCapability("acceptInsecureCerts", true);
            return driverClass.getDeclaredConstructor(Capabilities.class).newInstance(capabilities);
        } catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
            LOG.error("Cannot instantiate WebDriver for browser: '{}'", driverClass.getSimpleName(), e);
            throw new IllegalArgumentException("Cannot instantiate WebDriver for browser: " + driverClass.getSimpleName(), e);
        }
    }

    private Class<? extends RemoteWebDriver> setUpDriverInstance() {
        final String browserName = ConfController.getProperty(ConfController.BROWSER_KEY);
        if (BROWSER_DRIVERS.containsKey(browserName)) {
            Class<? extends RemoteWebDriver> driverClass = BROWSER_DRIVERS.get(browserName);
            WebDriverManager.getInstance(driverClass).setup();
            return driverClass;
        } else {
            throw new IllegalArgumentException("WebDriver is not supported yet for browser: " + browserName);
        }
    }

    private RemoteWebDriver initRemoteWebDriver() {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setBrowserName(ConfController.getProperty(ConfController.BROWSER_KEY));
        return new RemoteWebDriver(getSeleniumGridUrl(), capabilities);
    }

    private URL getSeleniumGridUrl() {
        String seleniumGridUrl = ConfController.getProperty(ConfController.SELENIUM_GRID);
        try {
            return new URL(seleniumGridUrl);
        } catch (MalformedURLException e) {
            LOG.error("Provided url is not correct: '{}'", seleniumGridUrl, e);
            throw new IllegalArgumentException("Provided url is not correct: " + seleniumGridUrl, e);
        }
    }
}
