package utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import dto.DtoObject;

import java.io.IOException;

public final class DtoReader {

    private DtoReader() {
        throw new IllegalStateException();
    }

    public static <T extends DtoObject> T getDto(String fileName, Class<T> dtoClass) {
        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        try {
            return mapper.readValue(ClassLoader.getSystemResourceAsStream(fileName), dtoClass);
        } catch (IOException e) {
            throw new IllegalArgumentException("Cannot read file: " + fileName, e);
        }
    }
}
