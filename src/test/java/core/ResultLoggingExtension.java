package core;

import com.google.common.base.Strings;
import org.junit.jupiter.api.extension.AfterTestExecutionCallback;
import org.junit.jupiter.api.extension.BeforeTestExecutionCallback;
import org.junit.jupiter.api.extension.ExtensionContext;

import java.lang.reflect.Method;

public class ResultLoggingExtension implements BeforeTestExecutionCallback, AfterTestExecutionCallback {

    private static final String CONSOLE_SIGN = "-";
    private static final int AMOUNT_OF_SIGNS = 101;

    @Override
    public void beforeTestExecution(ExtensionContext context) throws Exception {
        printTextInConsole("Running test: " + context.getRequiredTestMethod().getName());
        getStore(context).put(context.getRequiredTestMethod(), System.currentTimeMillis());
    }

    @Override
    public void afterTestExecution(ExtensionContext context) throws Exception {
        Method testMethod = context.getRequiredTestMethod();
        long start = getStore(context).remove(testMethod, long.class);
        long duration = System.currentTimeMillis() - start;
        printMessage(String.format("Execution of test took %s ms.", duration));

        boolean isPassed = !context.getExecutionException().isPresent();
        printTextInConsole(isPassed ? "TEST PASSED" : "TEST FAILED");
    }

    private ExtensionContext.Store getStore(ExtensionContext context) {
        return context.getStore(ExtensionContext.Namespace.create(getClass(), context));
    }

    private void printTextInConsole(String message) {
        printMessage("");
        printMessage(Strings.repeat(CONSOLE_SIGN, AMOUNT_OF_SIGNS));
        int amountOfSignsNearMessage = (AMOUNT_OF_SIGNS - message.length() - 2) / 2;
        printMessage(Strings.repeat(CONSOLE_SIGN, amountOfSignsNearMessage) + " " + message + " " + Strings.repeat(CONSOLE_SIGN, amountOfSignsNearMessage));
        printMessage(Strings.repeat(CONSOLE_SIGN, AMOUNT_OF_SIGNS));
        printMessage("");
    }

    @java.lang.SuppressWarnings("squid:S106")
    //reason: System.out.println used to distinguish printed message from logged messages
    private void printMessage(String print) {
        System.out.println(print);
    }
}
