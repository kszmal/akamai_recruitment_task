package core;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.extension.ExtendWith;

@ExtendWith(ResultLoggingExtension.class)
public interface AbstractUiTest {

    @AfterAll
    static void tearDown() {
        DriverFactory.getInstance().removeDriver();
    }

}
