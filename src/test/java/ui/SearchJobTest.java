package ui;

import core.AbstractUiTest;
import dto.JobBasicDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import pageobjects.JobResultsPanel;
import pageobjects.SearchJobPage;
import utils.DtoReader;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.core.Is.is;
import static org.junit.jupiter.api.Assertions.assertAll;

class SearchJobTest implements AbstractUiTest {

    private final SearchJobPage searchJobPage = new SearchJobPage();

    @BeforeEach
    void openSearchJobPage() {
        searchJobPage.openMainPage();
    }

    @Test
    @DisplayName("Un-logged customer is able to search for a job")
    void correctSearchCriteriaShouldReturnResults() {
        searchJobPage.enterSearchKeyword("Test");
        searchJobPage.selectLocation("Krakow, Poland");
        searchJobPage.clickSearch();

        JobResultsPanel results = searchJobPage.getJobResultsPanel();
        assertAll(
            () -> assertThat("Notification about no offers found shouldn't be displayed", results.isNoResultsMessageDisplayed(), is(false)),
            () -> assertThat("Any job offers should be found", results.getNumberOfResults(), greaterThan(0))
        );
    }

    @Test
    @DisplayName("Customer is notified when no offers match given criteria")
    void wrongSearchCriteriaShouldNotReturnResults() {
        searchJobPage.enterSearchKeyword("XXX");
        searchJobPage.clickSearch();
        JobResultsPanel results = searchJobPage.getJobResultsPanel();
        assertAll(
            () -> assertThat("Notification about no offers found should be displayed", results.isNoResultsMessageDisplayed(), is(true)),
            () -> assertThat("No job offers should be found", results.getNumberOfResults(), is(0))
        );
    }

    @Test
    @DisplayName("Job position for Senior Software Development Engineer in Test is available")
    void positionForSeniorSDETShouldBeDisplayed() {
        JobBasicDTO seniorSDET = DtoReader.getDto("seniorSDET.yml", JobBasicDTO.class);

        searchJobPage.enterSearchKeyword(seniorSDET.getJobId());
        searchJobPage.selectLocation(seniorSDET.getLocation());
        searchJobPage.clickSearch();

        JobResultsPanel resultsPanel = searchJobPage.getJobResultsPanel();
        List<JobBasicDTO> resultJobs = resultsPanel.getResultJobs();
        assertAll(
            () -> assertThat("Notification about no offers found shouldn't be displayed", resultsPanel.isNoResultsMessageDisplayed(), is(false)),
            () -> assertThat("There should be one job in result number", resultsPanel.getNumberOfResults(), is(1)),
            () -> assertThat("There should be one job in list", resultJobs.size(), is(1)),
            () -> {
                JobBasicDTO readJob = resultJobs.get(0);
                assertAll(
                    () -> assertThat("Job title should be correct", readJob.getJobTitle(), is(seniorSDET.getJobTitle())),
                    () -> assertThat("Location should be correct", readJob.getLocation(), is(seniorSDET.getLocation())),
                    () -> assertThat("Category should be correct", readJob.getCategory(), is(seniorSDET.getCategory())),
                    () -> assertThat("Description should be correct", readJob.getDescription(), is(seniorSDET.getDescription()))
                );
            }
        );
    }
}
