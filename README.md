# Akamai Recruitment Task

### Prerequisites

To run tests you need java 8, maven and dependencies associated with this test automation framework

## Running the tests

To run tests with default configuration (locally on chrome)
```
mvn test
```

To run tests locally on different browser
```
mvn test -Dbrowser=${browserName}
```
Note: used library 'WebDriverManager' should automate the Selenium Webdriver binaries management in runtime, but only Firefox and Chrome was tested, so only these two can be used

To run tests in selenium grid
```
mvn test -Dremote=true -DseleniumGrid=${seleniumGridUrl} -Dbrowser=${browserName}
```
Note: possible browser only depends on grid configuration

### And coding style tests


Project passed checkstyle, findbugs ans SonarQube static tests
```
mvn verify checkstyle:checkstyle findbugs:findbugs sonar:sonar
```
To use your sonar you have to provide SonarQube server url:  ```mvn verify sonar:sonar -Dsonar.host.url=${sonar url}```